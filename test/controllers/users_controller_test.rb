require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { email: 'kyle+user@fkbalderson.com', password: 'secret', password_confirmation: 'secret' } }, as: :json
    end

    assert_response 201
  end

  test "should show user" do
    get(
      me_url(@user), 
      headers: {
        Authorization: ActionController::HttpAuthentication::Basic.encode_credentials(@user.email, "secret")
      },
      as: :json
    )
    assert_response :success
  end

  test "should update user" do
    patch(
      user_url(@user),
      params: { user: { email: @user.email, password: 'secret', password_confirmation: 'secret' } },
      as: :json,
      headers: { Authorization: ActionController::HttpAuthentication::Basic.encode_credentials(@user.email, "secret") }
    )
    assert_response 200
  end

end
