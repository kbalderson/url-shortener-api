require "test_helper"

class LocatorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @locator = locators(:github)
    @user = users(:one)
  end

  test "should get index" do
    get locators_url, as: :json
    assert_response :unauthorized

    get(
      locators_url,
      as: :json,
      headers: {
        Authorization: ActionController::HttpAuthentication::Basic.encode_credentials(@user.email, "secret")
      }
    )
    assert_response :success
  end

  test "should create locator" do
    assert_difference('Locator.count') do
      post locators_url, params: { locator: { destination: @locator.destination } }, as: :json
    end

    assert_response 201

    post locators_url, params: { locator: { destination: @locator.destination, short_code: @locator.short_code } }, as: :json

    assert_response :unprocessable_entity
  end

  test "should destroy locator" do
    assert_difference('Locator.count', -1) do
      delete(
        locator_url(@locator),
        as: :json,
        headers: {
          Authorization: ActionController::HttpAuthentication::Basic.encode_credentials(@user.email, "secret")
        }
      )
    end

    assert_response 204
  end

  test "redirect" do
    locator = Locator.create(destination: 'https://example.com', short_code: '1jkg1D')

    get redirect_path(short_code: locator.short_code)
    assert_redirected_to(locator.destination, status: 301)

    get redirect_path(short_code: 'some-nonexistent-short-code')
    assert_response :not_found
  end
end
