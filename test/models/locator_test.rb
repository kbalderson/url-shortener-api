require "test_helper"

class LocatorTest < ActiveSupport::TestCase
  test "a short code is required" do
    locator = Locator.new(destination: 'http://example.com')
    assert_not locator.valid?
  end

  test "short codes must be unique" do
    locator1 = Locator.create(destination: 'http://example.com', short_code: 'short_code')
    locator2 = Locator.new(destination: 'http://example.com/2', short_code: 'short_code')

    assert_not locator2.save
  end

  test "destination must be a valid url" do
    locator = Locator.new(destination: 'yahoo')
    assert_not locator.valid?, 'Locator with bad url was valid'
    assert locator.errors.attribute_names.include?(:destination), "locator errors didn't include destination"
  end

  test "short codes must be url-safe" do
    locator = Locator.new(destination: "http://example.com", short_code: 'A0a81jks-fw_fjkw')
    assert locator.valid?, 'locator didn\'t allow permitted characters'

    locator = Locator.new(destination: "http://example.com", short_code: 'A***0a81jks-fw_fjkw')
    assert_not locator.valid?, "locator allowed asterisks"
  end

  test "short codes are case-sensitive" do
    locator1 = Locator.create(destination: 'http://example.com', short_code: 'short_code')
    locator2 = Locator.new(destination: 'http://example.com/2', short_code: 'short_code')
    locator3 = Locator.new(destination: 'http://example.com/3', short_code: 'short_Code')

    assert_not locator2.valid?

    assert locator3.valid?
  end

  test "a short code can be generated" do
    locator = Locator.new(destination: 'http://example.com')
    locator.generate_short_code
    assert locator.valid?
  end

  test "a short code collision raises an error" do
    Locator.stub_const(:SHORT_CODE_CHARACTERS, ['a']) do
      Locator.stub_const(:SHORT_CODE_LENGTH, 6..6) do
        Locator.create(destination: 'http://example.com', short_code: 'aaaaaa')

        assert_raises("A unique short code could not be generated") do
          Locator.safe_short_code
        end
      end
    end
  end
end
