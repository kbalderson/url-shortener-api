## Get Shorty - A URL shortener for people who want the exact same things that are offered by other url shortners.

### Initial Setup

```sh
cp .env{.sample,}
docker-compose run --rm web rails db:migrate
```

### Running the App

```sh
docker-compose up web
```

### Running Tests

```sh
docker-compose run --rm web rails db:test:prepare
docker-compose run --rm web rails test
```

### Using the API

The API generates its own documentation using the OpenAPI spec.

To retrieve a subset the OpenAPI JSON spec you can run the following:

```sh
curl -X OPTIONS http://localhost:3000/users/ | jq | pbcopy
# OR
curl -X OPTIONS http://localhost:3000/locators/ | jq | pbcopy
```

You can then paste the OpenAPI code into [insomnia](https://insomnia.rest) (or
another OpenAPI app) to try it out.

### Try out the demo

The app is currently hosted on Heroku, and available at: https://get-shorty-url-shortener.herokuapp.com/

You can fetch the API docs from that url and try it out without the need to run
the app locally.

```sh
curl -X OPTIONS https://get-shorty-url-shortener.herokuapp.com/users/ | jq | pbcopy
# OR
curl -X OPTIONS https://get-shorty-url-shortener.herokuapp.com/locators/ | jq | pbcopy
```

### About this Project

#### The Code

This project does not use service objects or form objects. I created it this way
for ease of readability and simplicity. Should the requirements become more
complex, I would refactor certain bits of code for consistency and readability.

I also tend not to use these types of abstractions until necessary because it
adds complexity to code, making onboarding take more time. It could also create
a barrier to entry for less-senior developers who haven't encountered those
patterns yet in their career.

I will stress, though, that when performance or code-readability suffer, I
advocate for abstraction and refactoring.

#### Back-end

From the outset, this project was designed to run in docker-compose. This should
make it portable for hosting in a [cloud-native
computing](https://en.wikipedia.org/wiki/Cloud_native_computing) environment,
and lessen the contamination of the machine on which it is being developed.

Because of the way MacOS and Windows share files with Docker in a VM, running it
in any environment other than Linux means that there may be a performance impact
for development, though this is less of a problem without node_modules.

#### Authentication

Rather than use something like [devise](https://github.com/heartcombo/devise) or
[clearance](https://github.com/thoughtbot/clearance), I opted for a very simple
authentication mechanism. There is a User model with a username and password
(which uses the rails built-in `has_secure_password` functionality), and a
simple session variable.

I did not add email address validation.

### Caveats

### What's next

There is a wide range of possibilities, but some of the immediate improvements I
see room for are:

**API Versioning**

I would like to work versioning into the `Accept` header, like this: `Accept:
application/vnd.example+json;version=1.0`.

**Documentation Improvements**

I would like to refactor the documentaion endpoints work similar to how they do
now, but to also generate a single OpenAPI file for the entire API.

It would also be good to document more non-successful responses from the API.

**Authentication**

It would be much more secure to have OAuth 2.0 with a JWT instead of HTTP Basic
Auth.

Also, and obviously, email addresses should be verified when the account is
created, and when the email address is changed by the user.

**Analytics**

Tracking clicks could be easy with a counter cache. But for more robust
reporting, I would probably opt for a separate model to keep track of clicks.
That would include IP address, time clicked.

**A front end**

A single-page app would be sufficient for this simple interface.

**Time-based Expiration**

Allow shortened urls to be expired automatically after a period of time
specified by the user
