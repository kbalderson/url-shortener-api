class Locator < ApplicationRecord
  SHORT_CODE_LENGTH = 6..12
  SHORT_CODE_CHARACTERS = ['a'..'z', 'A'..'Z', 0..9, '-', '_'].reduce([]) do |memo, range|
    memo + Array(range)
  end.freeze
  SHORT_CODE_GENERATION_ATTEMPTS = 5

  belongs_to :user, optional: true

  validates :destination, presence: true, format: {
    with: URI.regexp(%w[http https]),
    message: 'is not a valid URL'
  }

  validates :short_code, presence: true, uniqueness: true, format: {
    with: /\A[a-zA-Z0-9\-_]+\z/,
    message: 'must contain only letters, numbers, hyphens and underscores'
  }

  def generate_short_code
    self.short_code = Locator.safe_short_code
  end

  def self.safe_short_code
    # Generates a random short code and checks for collisions

    SHORT_CODE_GENERATION_ATTEMPTS.times do
      short_code = Array.new(rand(SHORT_CODE_LENGTH)) { Locator::SHORT_CODE_CHARACTERS.sample }.join()

      next if Locator.find_by(short_code: short_code)

      return short_code
    end

    raise 'A unique short code could not be generated'
  end

end
