require 'exceptions'

include ActionController::HttpAuthentication::Basic::ControllerMethods
include ActionController::HttpAuthentication::Token::ControllerMethods

class ApplicationController < ActionController::API
  helper_method :current_user

  rescue_from Exceptions::AuthorizationError, with: :render_unauthorized

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  def current_user
    authenticate_with_http_basic do |email, password|
      user = User.find_by_email email
      user if user && user.authenticate(password)
    end
  end


  def render_unauthorized
    render json: { error: 'unauthorized' }, status: :unauthorized
  end

  def render_not_found
    render json: { error: 'not_found' }, status: :not_found
  end

  def build_openapi_spec(models:, paths:)
    {
      openapi: '3.0.3',
      info: {
        title: 'Get Shorty',
        version: '0.0.1'
      },
      security: [
        { basicAuth: [] }
      ],
      servers: [
        {
          url: request.base_url,
          description: 'The Server'
        }
      ],
      components: {
        securitySchemes: {
          basicAuth: {
            type: :http,
            scheme: :basic
          }
        },
        schemas: {
        }.merge(models)
      },
      paths: paths
    }
  end

  def require_authentication
    raise Exceptions::AuthorizationError unless current_user
  end

end
