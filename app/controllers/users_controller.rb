class UsersController < ApplicationController
  before_action :set_user, only: %i[ show update destroy ]
  before_action :require_current_user, only: %i[ show update ]
  before_action :require_authentication, only: %i[ me ]

  def show; end

  def create
    @user = User.new(user_params)

    if @user.save
      render :show, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def me
    @user = current_user
    render :show
  end

  def update
    if @user.update(user_params)
      render :show, status: :ok, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def api_options
    models = {
      User: {
        type: :object,
        properties: {
          id: { type: :string, format: :uuid, readOnly: true },
          email: { type: :string, format: :email },
          password: { type: :string, format: :password, writeOnly: true },
          password_confirmation: { type: :string, format: :password, writeOnly: true },
        },
        required: %i[email password password_confirmation],
      }
    }

    paths = {
      '/me': {
        get: {
          description: 'Shows information about the user',
          responses: {
            '200': {
              description: 'Success',
              content: {
                'application/json': { schema: {'$ref': '#/components/schemas/User'} }
              }
            }
          }
        }
      },
      '/users': {
        post: {
          description: 'Create a user',
          responses: {
            '200': {
              description: 'Success',
              content: {
                'application/json': { schema: {'$ref': '#/components/schemas/User'} }
              }
            },
            '422': {
              description: 'The user could not be created'
            }
          },
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: :object,
                  properties: {
                    user: {
                      '$ref': '#/components/schemas/User'
                    }
                  }
                }
              }
            }
          },
        }
      },
      '/users/{id}': {
        patch: {
          description: 'update information about the user',
          responses: {
            '200': {
              description: 'Success',
              content: {
                'application/json': { schema: {'$ref': '#/components/schemas/User'} }
              }
            },
            '422': {
              description: 'The user could not be updated'
            }
          },
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: :object,
                  properties: {
                    user: {
                      '$ref': '#/components/schemas/User'
                    }
                  }
                }
              }
            }
          },
          "parameters": [
            {
              name: :id,
              in: :path,
              description: "ID of user",
              required: true,
              schema: {
                type: :array,
                items: { type: :string, format: :uuid }
              },
              style: :simple
            }
          ]

        }
      }
    }

    render json: build_openapi_spec(models: models, paths: paths)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end

    def require_current_user
      raise Exceptions::AuthorizationError unless current_user == @user
    end
end
