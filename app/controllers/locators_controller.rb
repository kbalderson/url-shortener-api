class LocatorsController < ApplicationController
  before_action :set_locator, only: %i[ destroy ]

  before_action :require_authentication, only: %i[ index show destroy ]

  def index
    @locators = current_user.locators
  end

  def create
    @locator = Locator.new(locator_params)

    @locator.user = current_user

    @locator.generate_short_code unless @locator.short_code

    if @locator.save
      render :show, status: :created, location: @locator
    else
      render json: @locator.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @locator.destroy
  end

  def api_options
    models = {
      Locator: {
        type: :object,
        properties: {
          id: { type: :string, format: :uuid, readOnly: true },
          destination: { type: :string, format: :url },
          short_code: { type: :string },
          user_id: { type: :string, format: :uuid, readOnly: true },
          short_url: { type: :string, format: :url, readOnly: true },
          created_at: { type: :string, format: :url, readOnly: true },
          updated_at: { type: :string, format: :url, readOnly: true },
          url: { type: :string, format: :url, readOnly: true },
        },
        required: %i[destination],
      }
    }

    paths = {
      '/shorten': {
        post: {
          description: 'Shortens a url',
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: :object,
                  properties: {
                    locator: { '$ref': '#/components/schemas/Locator' }
                  }
                }
              }
            }
          },
          responses: {
            '201': {
              description: 'Success',
              content: {
                'application/json': { schema: {'$ref': '#/components/schemas/Locator'} }
              }
            },
            '422': {
              description: 'The url could not be shortened'
            }

          }
        }
      },

      '/locators': {
        get: {
          description: 'View a list of all locators',
          responses: {
            '200': {
              description: 'Success',
              content: {
                'application/json': {
                  schema: {
                    type: 'array',
                    items: {
                      '$ref': '#/components/schemas/Locator'
                    }
                  }
                }
              }
            }
          },
        }
      },
      '/locators/{id}': {
        get: {
          description: 'View information about the Locator',
          responses: {
            '200': {
              description: 'Success',
              content: {
                'application/json': { schema: {'$ref': '#/components/schemas/Locator'} }
              }
            }
          } ,
          "parameters": [
            {
              name: :id,
              in: :path,
              description: "ID of Locator",
              required: true,
              schema: {
                type: :array,
                items: { type: :string, format: :uuid }
              },
              style: :simple
            }
          ]
        },
        delete: {
          description: 'Delete a locator',
          responses: {
            '204': {
              description: 'Success'
            }
          } ,
          "parameters": [
            {
              name: :id,
              in: :path,
              description: "ID of Locator",
              required: true,
              schema: {
                type: :array,
                items: { type: :string, format: :uuid }
              },
              style: :simple
            }
          ]

        }
      }
    }

    render json: build_openapi_spec(models: models, paths: paths)
  end

  def redirect
    @locator = Locator.find_by_short_code! params[:short_code]

    redirect_to @locator.destination, status: :moved_permanently
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_locator
      @locator = current_user.locators.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def locator_params
      params.require(:locator).permit(:destination, :short_code, :user_id)
    end
end
