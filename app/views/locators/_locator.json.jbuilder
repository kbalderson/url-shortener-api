json.extract! locator, :id, :destination, :short_code, :user_id, :created_at, :updated_at
json.short_url redirect_url(short_code: locator.short_code)
json.url locator_url(locator, format: :json)
