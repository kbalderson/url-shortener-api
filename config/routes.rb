Rails.application.routes.draw do
  root to: 'locators#api_options'

  resources :locators, only: [:index, :create, :destroy] do
    collection do
      match '', to: 'locators#api_options', via: :options, defaults: { format: :json }
    end
  end
  post 'shorten', to: 'locators#create'

  resources :users, only: [:show, :create, :update] do
    collection do
      match '', to: 'users#api_options', via: :options, defaults: { format: :json }
    end
  end

  get '/me', to: 'users#me', as: 'me'

  get '/:short_code', to: 'locators#redirect', as: 'redirect'

end
