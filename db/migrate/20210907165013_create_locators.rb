class CreateLocators < ActiveRecord::Migration[6.1]
  def change
    create_table :locators, id: :uuid do |t|
      t.string :destination
      t.string :short_code
      t.references :user, null: true, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
